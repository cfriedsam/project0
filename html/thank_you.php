<!--
This is the final page after submitting the order.
-->

<?php

session_start();
session_destroy();

require_once('../includes/helpers.php');

render('templates/header', array('title'=> 'Three Aces Pizza Service'));
render('thank_you');
render('templates/footer'); 

?>

