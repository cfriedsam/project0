<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title><?php echo htmlspecialchars($title) ?></title>
        <link type="text/css" href="styles.css" rel="stylesheet" />
    </head>
    <body>
        <header>
            <h1><a href="/"><?php echo htmlspecialchars($title) ?></a></h1>
        </header>
