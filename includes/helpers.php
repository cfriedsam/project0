<?php

/** Renders a template found in the templates folder
  * @param array $data
  */

function render($template, $data = array())
{
    $path = __DIR__ . '/../views/' . $template . '.php';
    if (file_exists($path))
    {
        extract($data);
        require($path);
    }
}

?>
  
