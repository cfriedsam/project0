<?php

session_start();

require_once('../includes/helpers.php');

$dom = simplexml_load_file("../model/menu.xml");
$categories = $dom->xpath("/menu/category");

// add stuff to SESSION
foreach($_POST as $key => $values) {
    //~ echo $key . "<br/>";
    //~ foreach($values as $k=>$v){
        //~ echo $k . ":" . $v ."<br/>";
    //~ }
    //~ exit;
    if (intval($values['amount']) > 0) {
        if (isset($_SESSION[$key]) && !isset($values['size'])) {
            $_SESSION[$key]['amount'] = $values['amount'];
        } else {
            $_SESSION[$key] = $values;
        }
    }
}

// redirect to index.php
header('Location: /');



?>
