<?php

session_start();

require_once('../includes/helpers.php');

$dom = simplexml_load_file("../model/menu.xml");
$categories = $dom->xpath("/menu/category");

render('templates/header', array('title'=> 'Three Aces Pizza Service'));
render('checkout', array('categories' => $categories, 'database' => $dom));
render('templates/footer'); 

?>
