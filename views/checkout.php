<?php require_once('../model/parse_order.php'); ?>

<section>
    <div class="row">
        <div id="cart">
            <h2>Thank you for your order</h2>
            
            <!-- list all items currently selected for checkout -->
            <p>Here a list of the items you have ordered today</p>
            <?php if (isset($_SESSION)) { 
                $total = 0;?>
                    <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>item</th>
                            <th>specifications</th>
                            <th>price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($_SESSION as $item => $values) {
                            $output = parse_order($item, $values, $database, $categories);?>
                            <tr>
                                <td><?php echo $output['amount']; ?></td>
                                <td><?php echo $output['item']; ?></td>
                                <td>
                                <?php foreach($output['attributes'] as $key => $value) {
                                    echo $value ." ";
                                } ?>
                                </td>
                                <td>$<?php echo number_format($output['price'],2); ?></td>
                            </tr>
                            <?php $total = $total + $output['price']; ?>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr class="total">
                            <td></td>
                            <td></td>
                            <td>Total:</td>
                            <td>$<?php echo number_format($total,2); ?></td>
                        </tr>
                    </tfoot>
                </table>
            <?php } ?>
            <strong>Your total for today's order is: $<?php echo number_format($total,2); ?></strong>
            
            <!-- dummy form for credit card processing -->
            <form id="checkout" action="thank_you.php">
                <div>
                    <label>Name</label>
                    <input type="text" />
                </div>
                <div>
                    <label>Address</label>
                    <input type="text" />
                </div>
                <div>
                    <label>City</label>
                    <input type="text" />
                </div>
                <div>
                    <label>Zip Code</label>
                    <input type="text" />
                </div>
                <div>
                    <label>Phone</label>
                    <input type="text" />
                </div>
                <div>
                    <label>Credit card type</label>
                    <input type="radio" name="type" /> Visa
                    <input type="radio" name="type" /> MasterCard
                </div>
                <div>
                    <label>Credit card number</label>
                    <input type="text" />
                </div>
                <div>
                    <label>Security code</label>
                    <input type="password" />
                </div>
                
                <div class="actions">
                    <input type="submit" value="Submit order" />
                </div>
            </form>
        </div>
    </div>
</section>
