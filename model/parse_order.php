<?php

function parse_order($item=array(), $values=array(), $database=array(), $categories=array())
{
    $output['amount'] = $values['amount'];
    preg_match('/(?P<name>\w+):(?P<digit>\d+)/',$item, $identifier);
    $selected_category= str_replace("_", ' ', $identifier['name']);
    $selected_item_id = $identifier['digit'];
    $output['price'] = 0;

    foreach($categories as $category){
        if($selected_category == $category->attributes()['name'])
        {
            foreach($category->item as $item)
            {
                if($selected_item_id == $item->attributes()['id'])
                {
                    $output['item']=$item->item_name;
                    foreach($values as $key => $value)
                    {
                        if($key != 'amount')
                        {
                            $output['attributes'][$key] = $value;
                            $output['price']= $output['price'] + floatval($item->price->$value)*100;
                        }
                    }
                }
            }
        }
    }
    $output['price'] = ($output['price'] * $output['amount'])/100;
    
    return $output;
}

?>
