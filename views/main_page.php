<?php require_once('../model/parse_order.php'); ?>

<section>
    <div class="row">
        <!-- Cart section which is displaying currently selected items -->
        <div id="cart">
            <h2>Cart</h2>
            
            <?php if (isset($_SESSION)) { 
                $total = 0;?>
                <form action="orders.php" method="post">
                    <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>item</th>
                            <th>specifications</th>
                            <th>price</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- output all items currently stored in session -->
                        <?php foreach($_SESSION as $item => $values) {
                            $output = parse_order($item, $values, $database, $categories);?>
                            <tr>
                                <td><input type="text" name="<? echo $item; ?>[amount]" value="<?php echo $output['amount']; ?>" /></td>
                                <td><?php echo $output['item']; ?></td>
                                <td>
                                <?php foreach($output['attributes'] as $key => $value) {
                                    echo $value ." ";
                                } ?>
                                </td>
                                <td>$<?php echo number_format($output['price'],2); ?></td>
                                <td><a href="remove.php?id=<?php echo $item ?>">X</a></td>
                            </tr>
                            <?php $total = $total + $output['price']; ?>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <!-- output total charges -->
                        <tr class="total">
                            <td><input type="submit" value="Update" /></td>
                            <td></td>
                            <td>Total:</td>
                            <td>$<?php echo number_format($total,2); ?></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
                </form>
                <a href="/checkout.php" class="button checkout">Place Order</a>
                <a href="/reset.php" class="button clear">Empty Cart</a>
            <?php } ?>
        </div>
    
        <!-- Menu section displays part of the menu depending on the passed 'tab' parameter -->
        <div id="menu">
            <h2>Menu</h2>
            <form action="orders.php" method="post">
                <div id="tabs">
                    <?php
                    // collect all possible tabs
                    $tabs = array();
                    $active_tab = isset($_GET['tab']) ? $_GET['tab'] : null;
                    foreach($categories as $category) {
                        $tab_name = $category->attributes()['cat']."";
                        $tabs[$tab_name] = true;
                        $active_tab = isset($active_tab) ? $active_tab : $tab_name;
                    } ?>
                    <ul>
                    <?php
                    // output all tabs in a list
                    foreach ($tabs as $tab => $value) {
                        $class = (strcmp($active_tab, $tab) == 0) ? 'active' : '';
                        echo "<li class=\"tabs $class\"><a href=\"?tab=$tab\">$tab</a></li>";
                    } ?>
                    </ul>
                    
                    <?php
                    // iterate over all categories and make the 'tab' active if the cat attribute is matching the active tab
                    $current_tab = "";
                    foreach ($categories as $category)
                    {
                        if ($current_tab != $category->attributes()['cat']) {
                            $current_tab = $category->attributes()['cat'];
                        }
                        $name = $category->attributes()['name'];
                    ?>
                    <div class="tab <?php if (strcmp($active_tab, $current_tab) == 0) { echo "active"; } ?>">
                        <h3><?php  echo $name; ?></h3>
                        <?php if ($category->remarks != null) { ?>
                        <p><?php echo $category->remarks; ?></p>
                        <?php } ?>
                        <table> 
                            <thead>
                                <tr>
                                    <?php 
                                    foreach($category->headers->attributes() as $a => $b)
                                    {
                                    ?>
                                        <th><?php print $b; ?></th>
                                    <?php
                                    } 
                                    ?>
                                    <th class="quantity">#</th>
                                </tr>    
                            </thead>
                            <tbody>
                                <?php 
                                foreach($category->item as $item)
                                { 
                                    $attrs = $item->attributes();
                                    $item_id = $attrs['id'];
                                ?>
                                <tr>
                                    <?php
                                    foreach($item->children() as $col)
                                    {
                                        if($col->getName()=="price")
                                        {
                                            foreach($col->children() as $option)
                                            {
                                            ?>
                                                <td>
                                                <?php if ($option != "") {
                                                    if($option->attributes()['type']=="size"){
                                                        echo "<input type=\"radio\" name=\"".
                                                            $name .":". $item_id."[size]\" ".
                                                            "value=\"".$option->getName()."\" checked/>";
                                                        echo $option;
                                                    }
                                                    else{
                                                        echo "<input type=\"checkbox\" name=\"".
                                                            $name .":". $item_id."[".$option->getName()."]\" ".
                                                            "value=\"".$option->getName()."\" />";
                                                        echo $option;
                                                    }
                                                } ?>
                                                </td>
                                        <?php
                                            }
                                        }
                                        else
                                        {
                                        ?>
                                            <td><?php print $col; ?></td>
                                    <?php
                                        }
                                    }                           
                                    ?>
                                    <td><input type="text" name="<?php 
                                        echo $name .":". $item_id; ?>[amount]" />
                                    </td>
                                </tr>
                            <?php
                             } 
                            ?>
                            </tbody>
                        </table>
                    </div>        
                    <?php 
                    } 
                    ?>
                    <div class="actions">
                        <input type="submit" value="Add to Order" />
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>
